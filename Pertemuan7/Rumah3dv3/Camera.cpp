/************************************
**                                 **
** Program by Muhammad Bagus Zulmi **
**  https://mbaguszulmi.github.io  **
**                                 **
************************************/


#define _USE_MATH_DEFINES

#include "Camera.h"
#include <stdlib.h>
#include <GL/glut.h>
#include <math.h>
#include <stdio.h>

Camera::Camera(GLfloat eyeX, 
                GLfloat eyeZ, 
                GLfloat distance, 
                GLfloat rotateSpeed, GLfloat moveSpeed):
                eyeX(eyeX),
                eyeZ(eyeZ),
                distance(distance),
                rotateSpeed(rotateSpeed),
                moveSpeed(moveSpeed) {
    
    angle = 0;
    origin = 0;
    hangle = 0;
    eyeY = 0;
    centerX = 0;
    centerZ = eyeZ-distance;
    centerY = 0;
    viewChanged = false;
}

GLfloat Camera::degToRad(GLfloat deg) {
    return deg*M_PI/180.0;
}

void Camera::key(unsigned char key) {
    GLfloat rad = degToRad(origin+(45*curRotateY)),
            cosRes, sinRes;

    cosRes = cos(rad)*moveSpeed;
    sinRes = sin(rad)*moveSpeed;
    switch (key)
    {
        case 'w':
            if (viewChanged) {
                angle = origin + (45*curRotateY);
                viewChanged = false;
            }
            eyeZ-=cos(rad)*moveSpeed*curMove;
            eyeX+=sin(rad)*moveSpeed*curMove;
            centerZ-=cosRes;
            centerX+=sinRes;
            rotateView();

            break;
        case 's':
            eyeZ+=cosRes;
            eyeX-=sinRes;
            centerZ+=cosRes;
            centerX-=sinRes;
            break;
        case 'a':
            eyeX-=cosRes;
            eyeZ-=sinRes;
            centerX-=cosRes;
            centerZ-=sinRes;
            break;
        case 'd':
            eyeX+=cosRes;
            eyeZ+=sinRes;
            centerX+=cosRes;
            centerZ+=sinRes;
            break;

        default:
            break;
    }

    printf("Moving with eyeX %f, eyeZ %f, %f degree angle\n", eyeX, eyeZ, angle);
}

void Camera::setCamera() {
    gluLookAt(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, 0, 1, 0);
}

void Camera::setMaxXY(GLfloat maxx, GLfloat maxy) {
    maxX = maxx;
    maxY = maxy;
}

void Camera::mouseMove(int x, int y) {
    GLfloat rad, distance2d;

    viewChanged = true;

    curRotateY = x/maxX;
    curRotateX = y/maxY;
    curMove = (maxX-abs(x))/maxX;
    hangle = 45*curRotateX;

    rad = degToRad(hangle);
    centerY = sin(rad)*distance;
    distance2d = cos(rad)*distance;
    if (distance2d < 0) distance2d*=-1;

    rad = degToRad(origin + (45*curRotateY));
    centerZ=eyeZ-(cos(rad)*distance2d);
    centerX=eyeX+(sin(rad)*distance2d);
}

void Camera::rotateView() {
    GLfloat rad, distance2d;
    angle+=rotateSpeed*curRotateY;
    origin+=rotateSpeed*curRotateY;

    rad = degToRad(hangle);
    centerY = sin(rad)*distance;
    distance2d = cos(rad)*distance;
    if (distance2d < 0) distance2d*=-1;

    rad = degToRad(angle);
    centerZ=eyeZ-(cos(rad)*distance2d);
    centerX=eyeX+(sin(rad)*distance2d);
}