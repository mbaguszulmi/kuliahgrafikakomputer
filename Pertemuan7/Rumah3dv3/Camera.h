/************************************
**                                 **
** Program by Muhammad Bagus Zulmi **
**  https://mbaguszulmi.github.io  **
**                                 **
************************************/


#include <stdlib.h>
#include <GL/glut.h>

class Camera
{
private:
    GLfloat angle, hangle, eyeX, eyeY, eyeZ, rotateSpeed, centerX, centerY, centerZ, distance, moveSpeed;
    GLfloat maxX, maxY, curRotateX, curRotateY, curMove, origin;
    bool viewChanged;
public:
    Camera(GLfloat eyeX, GLfloat eyeZ, GLfloat distance, GLfloat rotateSpeed, GLfloat moveSpeed);
    GLfloat degToRad(GLfloat deg);
    void key(unsigned char key);
    void setCamera();
    void setMaxXY(GLfloat maxx, GLfloat maxy);
    void mouseMove(int x, int y);
    void rotateView();
};
