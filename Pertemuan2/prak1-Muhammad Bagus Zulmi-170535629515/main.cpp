/*
 * GLUT Shapes Demo
 *
 * Written by Nigel Stewart November 2003
 *
 * This program is test harness for the sphere, cone
 * and torus shapes in GLUT.
 *
 * Spinning wireframe and smooth shaded shapes are
 * displayed until the ESC or q key is pressed.  The
 * number of geometry stacks and slices can be adjusted
 * using the + and - keys.
 */

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>

static int slices = 16;
static int stacks = 16;

/* GLUT callback Handlers */

void renderScene() {
    glClear(GL_COLOR_BUFFER_BIT);
//    glBegin(GL_TRIANGLES);
//        glVertex3f(-0.5, -0.5, 0);
//        glVertex3f(0.5, 0, 0);
//        glVertex3f(0, 0.5, 0);
//    glEnd();

    glBegin(GL_TRIANGLES);
        glVertex3f(-50, -50, 0);
        glVertex3f(0, -50, 0);
        glVertex3f(-50, 0, 0);
    glEnd();

    glBegin(GL_TRIANGLES);
        glVertex3f(50, -50, 0);
        glVertex3f(50, 0, 0);
        glVertex3f(0, -50, 0);
    glEnd();

    glBegin(GL_TRIANGLES);
        glVertex3f(50, 50, 0);
        glVertex3f(0, 50, 0);
        glVertex3f(50, 0, 0);
    glEnd();

    glBegin(GL_TRIANGLES);
        glVertex3f(-50, 50, 0);
        glVertex3f(-50, 0, 0);
        glVertex3f(0, 50, 0);
    glEnd();

    glBegin(GL_TRIANGLES);
        glVertex3f(0, 0, 0);
        glVertex3f(-50, 0, 0);
        glVertex3f(0, -50, 0);
    glEnd();
    glFlush();
}


static void key(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 27 :
        case 'q':
            exit(0);
            break;

        case '+':
            slices++;
            stacks++;
            break;

        case '-':
            if (slices>3 && stacks>3)
            {
                slices--;
                stacks--;
            }
            break;
    }

    glutPostRedisplay();
}

static void idle(void)
{
    glutPostRedisplay();
}

const GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 1.0f };
const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_position[] = { 2.0f, 5.0f, 5.0f, 0.0f };

const GLfloat mat_ambient[]    = { 0.7f, 0.7f, 0.7f, 1.0f };
const GLfloat mat_diffuse[]    = { 0.8f, 0.8f, 0.8f, 1.0f };
const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat high_shininess[] = { 100.0f };

/* Program entry point */

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_DEPTH | GLUT_SINGLE | GLUT_RGBA);
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(640, 640);

    glutCreateWindow("GLUT : Muhammad Bagus Zulmi");
    gluOrtho2D(-50, 50, -50, 50);
    glutDisplayFunc(renderScene);
    glutMainLoop();

    return EXIT_SUCCESS;
}
