#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <math.h>

const int width = 640,
          height = 360;


void drawBall(int posX, int posY, int r) {
    int rep = 0, cr = 0;
    float cy;

    glBegin(GL_TRIANGLE_FAN);
        glColor3f(1, 1, 1);
        glVertex2f(posX, posY);

        glColor3f(1, 0, 0);
        while(true) {
            cy = sqrt(pow(r, 2)-pow(cr, 2));

            if (rep > 0 && rep < 3) cy = -cy;

            glVertex2f(posX+cr, posY+cy);

            if (rep < 2) {
                if (rep%2 == 0) cr++;
                else cr--;
            }
            else {
                if (rep%2 == 0) cr--;
                else cr++;
            }

            if (rep == 4) break;
            else if (cr == r || cr == -r || cr == 0) rep++;
        }
    glEnd();
}

GLfloat posX=0, posY=0;
int r = 50;
bool top = true, right = true;

static void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
    glPushMatrix();
        glTranslatef(posX, posY, 0);
        drawBall(0, 0, r);
    glPopMatrix();

    if (top) posY++;
    else posY--;

    if (right) posX++;
    else posX--;

    if (posX <= -(width/2)+r || posX >= (width/2)-r) right = !right;
    if (posY <= -(height/2)+r || posY >= (height/2)-r) top = !top;
    glFlush();
}

void timer(int v) {
    glutPostRedisplay();
    glutTimerFunc(1000/30, timer, 0);
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(width,height);
    glutInitWindowPosition(50,50);
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE| GLUT_DEPTH);

    glutCreateWindow("Bouncing Ball");
    glClearColor(0.16863, 0.17647, 0.21176, 0);
    gluOrtho2D(-width/2, width/2, -height/2, height/2);
    glutDisplayFunc(display);
    glutTimerFunc(1000/30, timer, 0);
    glutMainLoop();

    return EXIT_SUCCESS;
}
