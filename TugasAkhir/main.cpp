#include <GL/glut.h>
// #include <stdlib.h>
// #include <iostream>
#include <stdio.h>
#include "glm.h"
#include "Imageloader.h"
#include "Camera.h"

// global
    GLfloat angleX = 33, angleY = 0, defAngle = 0, px = 0, py = -2, pz = -20, sw, sh;
    GLuint texRumput, texBrick, texBata, texMenu, texAbout, texBack;
    int page = 0;
    bool interact = false;
    // 0 = menu
    // 1 = Start
    // 2 = Lihat
    // 3 = about
    Camera camera(0, 10, 10, 2, 0.1);
// end

void resize(int w, int h) {
    const float ar = (float) w / (float) h;
    sw = w;
    sh = h;
    glutReshapeWindow(w, (w*9)/16);
    camera.setMaxXY(sw/2, sh/2);
    printf("window w : %d\nWindow h : %d\n", w, h);

    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, ar, 1, 100);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

GLMmodel *fountain = NULL,
         *pohon = NULL,
         *rumput = NULL,
         *setapak = NULL,
         *labirin = NULL,
         *kursi = NULL,
         *lampu = NULL;

void drawMenu() {
    GLfloat mw = 16,
            mh = 9;

    glTranslatef(0, 0, -10.8);
    glBindTexture(GL_TEXTURE_2D, texMenu);

    glBegin(GL_QUADS);
        glNormal3f(0, 0, 1);
        glTexCoord2f(0, 1);
        glVertex2f(-mw/2, mh/2);
        glTexCoord2f(0, 0);
        glVertex2f(-mw/2, -mh/2);
        glTexCoord2f(1, 0);
        glVertex2f(mw/2, -mh/2);
        glTexCoord2f(1, 1);
        glVertex2f(mw/2, mh/2);
    glEnd();
}

void drawAbout() {
    GLfloat mw = 16,
            mh = 9;

    glTranslatef(0, 0, -10.8);
    glBindTexture(GL_TEXTURE_2D, texAbout);

    glBegin(GL_QUADS);
        glNormal3f(0, 0, 1);
        glTexCoord2f(0, 1);
        glVertex2f(-mw/2, mh/2);
        glTexCoord2f(0, 0);
        glVertex2f(-mw/2, -mh/2);
        glTexCoord2f(1, 0);
        glVertex2f(mw/2, -mh/2);
        glTexCoord2f(1, 1);
        glVertex2f(mw/2, mh/2);
    glEnd();
}

void drawBack() {
    // glTranslatef(-6.099997, 3.399999, -9.700004);
    glTranslatef(60, 630, 1);
    glBindTexture(GL_TEXTURE_2D, texBack);

    glBegin(GL_QUADS);
        glNormal3f(0, 0, 1);
        glTexCoord2f(0, 1);
        glVertex2f(-50, 25);
        glTexCoord2f(0, 0);
        glVertex2f(-50, -35);
        glTexCoord2f(1, 0);
        glVertex2f(50, -35);
        glTexCoord2f(1, 1);
        glVertex2f(50, 25);
    glEnd();
}

void drawModelFountain() {
    if (!fountain) {
        fountain = glmReadOBJ("/home/mbaguszulmi/Documents/SMT4/Grafika-Komputer/kuliahgrafikakomputer/TugasAkhir/obj/fountain.obj");
        if (!fountain) exit(0);
        glmScale(fountain, 2);
        glmFacetNormals(fountain);
        glmVertexNormals(fountain, 90.0);
    }

    glmDraw(fountain, GLM_SMOOTH | GLM_MATERIAL);
}

void drawModelPohon() {
    if (!pohon) {
        pohon = glmReadOBJ("/home/mbaguszulmi/Documents/SMT4/Grafika-Komputer/kuliahgrafikakomputer/TugasAkhir/obj/pohon.obj");
        if (!pohon) exit(0);
        glmScale(pohon, 2);
        glmFacetNormals(pohon);
        glmVertexNormals(pohon, 90.0);
    }

    glmDraw(pohon, GLM_SMOOTH | GLM_MATERIAL);
}

void drawModelRumput() {
    if (!rumput) {
        rumput = glmReadOBJ("/home/mbaguszulmi/Documents/SMT4/Grafika-Komputer/kuliahgrafikakomputer/TugasAkhir/obj/rumput.obj");
        if (!rumput) exit(0);
        glmScale(rumput, 2);
        glmFacetNormals(rumput);
        glmVertexNormals(rumput, 90.0);
    }

    glBindTexture(GL_TEXTURE_2D, texRumput);
    glmDraw(rumput, GLM_SMOOTH | GLM_TEXTURE);
}

void drawModelSetapak() {
    if (!setapak) {
        setapak = glmReadOBJ("/home/mbaguszulmi/Documents/SMT4/Grafika-Komputer/kuliahgrafikakomputer/TugasAkhir/obj/setapak.obj");
        if (!setapak) exit(0);
        glmScale(setapak, 2);
        glmFacetNormals(setapak);
        glmVertexNormals(setapak, 90.0);
    }

    glBindTexture(GL_TEXTURE_2D, texBrick);
    glmDraw(setapak, GLM_SMOOTH | GLM_TEXTURE);
}

void drawModelLabirin() {
    if (!labirin) {
        labirin = glmReadOBJ("/home/mbaguszulmi/Documents/SMT4/Grafika-Komputer/kuliahgrafikakomputer/TugasAkhir/obj/labirin.obj");
        if (!labirin) exit(0);
        glmScale(labirin, 2);
        glmFacetNormals(labirin);
        glmVertexNormals(labirin, 90.0);
    }

    glBindTexture(GL_TEXTURE_2D, texBata);
    glmDraw(labirin, GLM_SMOOTH | GLM_TEXTURE);
}

void drawModelKursi() {
    if (!kursi) {
        kursi = glmReadOBJ("/home/mbaguszulmi/Documents/SMT4/Grafika-Komputer/kuliahgrafikakomputer/TugasAkhir/obj/kursi.obj");
        if (!kursi) exit(0);
        glmScale(kursi, 2);
        glmFacetNormals(kursi);
        glmVertexNormals(kursi, 90.0);
    }

    glmDraw(kursi, GLM_SMOOTH);
}

void drawModelLampu() {
    if (!lampu) {
        lampu = glmReadOBJ("/home/mbaguszulmi/Documents/SMT4/Grafika-Komputer/kuliahgrafikakomputer/TugasAkhir/obj/lampu2.obj");
        if (!lampu) exit(0);
        // glmReverseWinding(lampu);
        glmScale(lampu, 2);
        glmFacetNormals(lampu);
        glmVertexNormals(lampu, 90.0);
    }

    glmDraw(kursi, GLM_SMOOTH | GLM_MATERIAL);
}


void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, sw/sh, 1, 100);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    switch (page)
    {
        case 0:
            glPushMatrix();
                drawMenu();
            glPopMatrix();
            break;
        case 1:
            // start
            glPushMatrix();
                camera.setCamera();
                glTranslatef(0, -1.5, -20);
                // glRotatef(angleX, 1, 0, 0);
                glRotatef(180, 0, 1, 0);

                drawModelFountain();
                drawModelKursi();
                drawModelPohon();
                drawModelRumput();
                drawModelSetapak();
                drawModelLabirin();
            glPopMatrix();
            break;
        case 2:
            // lihat
            glPushMatrix();
                glTranslatef(px, py, pz);
                glRotatef(angleX, 1, 0, 0);

                if (interact) {
                    glRotatef(angleY, 0, 1, 0);
                }
                else {
                    glRotatef(angleY++, 0, 1, 0);
                }

                drawModelFountain();
                drawModelKursi();
                drawModelPohon();
                drawModelRumput();
                drawModelSetapak();
                drawModelLabirin();
            glPopMatrix();
            break;
        case 3:
            // about
            glPushMatrix();
                drawAbout();
            glPopMatrix();
            break;
    
        default:
            break;
    }

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, 1174, 0, 660);
    // gluOrtho2D(-sw/2, sw/2, -sh/2, sh/2);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    if (page != 0) {
        glPushMatrix();
            drawBack();
        glPopMatrix();
    }    

    glDisable(GL_TEXTURE);

    glutSwapBuffers();
}

void idle() {
    glutPostRedisplay();
}

void getImage() {
    Image *img = loadBMP("/home/mbaguszulmi/Documents/SMT4/Grafika-Komputer/kuliahgrafikakomputer/TugasAkhir/texture/grass.bmp");
    Image *img2 = loadBMP("/home/mbaguszulmi/Documents/SMT4/Grafika-Komputer/kuliahgrafikakomputer/TugasAkhir/texture/brick.bmp");
    Image *img3 = loadBMP("/home/mbaguszulmi/Documents/SMT4/Grafika-Komputer/kuliahgrafikakomputer/TugasAkhir/texture/bata.bmp");
    Image *img4 = loadBMP("/home/mbaguszulmi/Documents/SMT4/Grafika-Komputer/kuliahgrafikakomputer/TugasAkhir/texture/menufix.bmp");
    Image *img5 = loadBMP("/home/mbaguszulmi/Documents/SMT4/Grafika-Komputer/kuliahgrafikakomputer/TugasAkhir/texture/aboutfix.bmp");
    Image *img6 = loadBMP("/home/mbaguszulmi/Documents/SMT4/Grafika-Komputer/kuliahgrafikakomputer/TugasAkhir/texture/backbtn.bmp");
    loadTextures(img, &texRumput, 1);
    loadTextures(img2, &texBrick, 1);
    loadTextures(img3, &texBata, 1);
    loadTextures(img4, &texMenu, 1);
    loadTextures(img5, &texAbout, 1);
    loadTextures(img6, &texBack, 1);
}

void keyboard(unsigned char k, int x, int y) {
    if (page == 2) {
        interact = true;
        switch (k)
        {
            case 'a':
                angleY--;
                break;

            case 'd':
                angleY++;
                break;
            
            case 'w':
                angleX++;
                break;

            case 's':
                angleX--;
                break;
            
            case 'i':
                pz++;
                break;
            
            case 'o':
                pz--;
                break;
        
            default:
                break;
        }
    }
    else if (page == 1) {
        camera.key(k);
    }
    
    if (page != 0 && k == 27) {
        page = 0;
    }
    
    
    
    // printf("px : %f\npy : %f\npz : %f\nangleX : %f\nangleY : %f\n", px, py, pz, angleX, angleY);
}

void special(int k, int x, int y) {
    if (page == 2) {
        interact = true;
        
        switch (k)
        {
            case GLUT_KEY_UP:
                py++;
                break;

            case GLUT_KEY_DOWN:
                py--;
                break;
            
            case GLUT_KEY_LEFT:
                px--;
                break;
            
            case GLUT_KEY_RIGHT:
                px++;
                break;
        
            default:
                break;
        }
    }
}

void passiveMotion(int x, int y) {
    x -= sw/2;
    y = (sh/2) - y;

    if (page == 1) {
        camera.mouseMove(x, y);
    }

    // printf("Mouse x : %d\nMouse y : %d\n", x, y);
}

void mouse(int button, int state, int x, int y) {
    if (page == 0) {
        switch (button)
        {
            case GLUT_LEFT_BUTTON:
                switch (state)
                {
                    case GLUT_UP:
                        if (x >= sw*0.415 && y >= sh*0.25 && x <= sw*0.594 && y <= sh*0.367) {
                            // Click on start button
                            printf("Start\n");
                            page = 1;
                        }
                        else if (x >= sw*0.412 && y >= sh*0.458 && x <= sw*0.588 && y <= sh*0.573) {
                            // Click on lihat button
                            printf("Lihat\n");
                            page = 2;
                        }
                        else if (x >= sw*0.412 && y >= sh*0.659 && x <= sw*0.588 && y <= sh*0.771) {
                            // Click on about button
                            printf("About\n");
                            page = 3;
                        }
                        
                        break;
                
                    default:
                        break;
                }
                break;
        
            default:
                break;
        }
    }
    else {
        if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
            if (x >= sw*0.0058 && y >= sh*0.0208 && x <= sw*0.1433 && y <= sh*0.138) {
                // Click on about button
                printf("Back\n");
                page = 0;
            }
        }
        
    }
}

void keyboardUp(unsigned char key, int x, int y) {
    if (page == 2) {
        interact = false;
    }   
}

void specialUp(int key, int x, int y) {
    if (page == 2) {
        interact = false;
    }
    
}

void init() {
    GLfloat lightPos[] = {0, 5, 0.1, 20, 0};
    GLfloat lightAmbient[] = {1, 1, 1, 1};
    GLfloat lightDiffuse[] = {1, 1, 1, 1};
    GLfloat lightSpecular[] = {0.5, 0.5, 0.5, 1};
    GLfloat shine[] = {80};

    glClearColor(0.33725, 0.784314, 0.921569, 1);

    glClearDepth(1);
    glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);

    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);

    getImage();
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(1174, 1);
    glutInitWindowPosition(10, 10);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

    glutCreateWindow("Load OBJ");
    glutReshapeFunc(resize);
    glutDisplayFunc(display);
    glutIdleFunc(idle);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(special);
    glutPassiveMotionFunc(passiveMotion);
    glutMouseFunc(mouse);
    glutKeyboardUpFunc(keyboardUp);
    glutSpecialUpFunc(specialUp);

    init();
    glutMainLoop();
    return 0;
}
